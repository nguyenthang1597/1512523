import React from 'react'
import { Router, Stack, Scene, Tabs } from 'react-native-router-flux';
import { connect } from 'react-redux'
import AppBar from '../Component/Appbar'
import Login from '../Container/Login';
import Home from '../Container/Home'
import TabIcon from '../Component/TabIcon'
import Icon from 'react-native-vector-icons/FontAwesome';
import Category from '../Component/Category'
import Basket from '../Container/Basket'
import MerchantDetail from '../Component/MerchantDetail'
import FoodDetail from '../Container/FoodDetail'
import Info from '../Container/Info'
import Notification from '../Container/Notification'
import Map from '../Component/Map';
import I18n from '../I18n'
import Setting from '../Container/Setting';
import ChangePassword from '../Container/ChangePassword';
import Order from '../Container/Order'
import Search from '../Component/Search'
import OrderHistory from '../Container/OrderHistory';
import OrderDetail from '../Container/OrderDetail';
import Comments from '../Container/Comments'
import PushNotification from 'react-native-push-notification';
import { firebase } from '../Config'
import { fnPostToken } from '../API/PostToken';
import SignUp from '../Component/Signup';
import ForgetPassword from '../Component/ForgetPassword';
class AppRouter extends React.Component {
  componentDidMount = () => {
    I18n.locale = this.props.language;
    if (this.props.token) {
      firebase.messaging().getToken().then((token) => {
        return fnPostToken(this.props.token, token, 'android').catch(e => {
          console.error(e)
        })
      });

      firebase.messaging().onTokenRefresh((token) => {
        return fnPostToken(this.props.token, token, 'android').catch(e => console.log(e))
      });
    }

  };

  componentWillReceiveProps(newProps) {
    if (newProps.language !== this.props.language)
      I18n.locale = newProps.language;
  }

  render() {
    return (
      <Router>
        <Scene key='root'>
          <Scene key='login' component={Login} initial hideNavBar />
          <Tabs key='Main' showLabel={false} hideNavBar >
            <Scene key='home' title={I18n.t('homepageTitle')} map component={Home} navBar={AppBar} icon={TabIcon} displayIcon={<Icon name='home' size={30} />} />
            <Scene key='search' title={I18n.t('search')} map component={Search} navBar={AppBar} icon={TabIcon} displayIcon={<Icon name='search' size={30} />} />
            <Scene key='basket' title={I18n.t('yourCart')} map component={Basket} navBar={AppBar} icon={TabIcon} displayIcon={<Icon name='shopping-cart' size={30} />} />
            <Scene key='noti' title={I18n.t('notification')} map component={Notification} navBar={AppBar} icon={TabIcon} displayIcon={<Icon name='bell' size={30} />} />
            <Scene key='info' title={I18n.t('userInfo')} map component={Info} navBar={AppBar} icon={TabIcon} displayIcon={<Icon name='user' size={30} />} />
            <Scene key='setting' title={I18n.t('setting')} map component={Setting} navBar={AppBar} icon={TabIcon} displayIcon={<Icon name='cog' size={30} />} />
          </Tabs>
          <Scene key='comments' title={I18n.t('comment')} component={Comments} back navBar={AppBar} />
          <Scene key='category' component={Category} back navBar={AppBar} />
          <Scene key='orderdetail' component={OrderDetail} title={I18n.t('orderDetail')} back navBar={AppBar} />
          <Scene key='merchantdetail' title={I18n.t('merchantDetail')} component={MerchantDetail} back navBar={AppBar} />
          <Scene key='fooddetail' title={I18n.t('detail')} component={FoodDetail} back navBar={AppBar} />
          <Scene key='order' title={I18n.t('orderTitle')} component={Order} back navBar={AppBar} />
          <Scene key='map' title={I18n.t('map')} component={Map} back navBar={AppBar} />
          <Scene key='orderhistory' title={I18n.t('orderhistory')} component={OrderHistory} back navBar={AppBar} />
          <Scene key='changepassword' title={I18n.t('changepasswordTitle')} component={ChangePassword} back navBar={AppBar} />
          <Scene key='signup' title={I18n.t('signup')} component={SignUp} back hideNavBar />
          <Scene key='forgetpw' title={I18n.t('forgetPassword')} component={ForgetPassword} back hideNavBar />
        </Scene>
      </Router>
    )
  }
}


const mapStateToProps = ({ Authenticate: { token }, Setting: { language } }) => ({ token, language })

export default connect(mapStateToProps)(AppRouter);