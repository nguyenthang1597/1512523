import {connect} from 'react-redux'
import OrderDetail from '../Component/OrderDetail';


const mapStateToProps = ({Authenticate: {token}}) => ({token})

export default connect(mapStateToProps)(OrderDetail);