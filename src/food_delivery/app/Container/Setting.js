import {changeLanguage, changeNotification} from '../Action/Setting';
import {logout} from '../Action/Authenticate'
import {connect} from 'react-redux'
import Setting from '../Component/Setting';
import { clear } from '../Action/Basket';

const mapStateToProps = ({Setting: {language, notification}}) => ({
  language,
  notification
})

const mapDispatchToProps = dispatch => ({
  changeLanguage: language => dispatch(changeLanguage(language)),
  logout: () => dispatch(logout()),
  changeNotification: () => dispatch(changeNotification()),
  clear: () => dispatch(clear())
})

export default connect(mapStateToProps, mapDispatchToProps)(Setting)