import {connect} from 'react-redux';
import FoodDetail from '../Component/FoodDetail';
import {addItem} from '../Action/Basket'

const mapStateToProps = ({Basket: {idRes}}) => ({idRes})

const mapDispatchToProps = dispatch => {
  return {
    addItem: (item, resId) => {
      dispatch(addItem(item, resId));
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(FoodDetail);