import {connect} from 'react-redux'
import Basket from '../Component/Basket';
import {increase, decrease, deleteItem} from '../Action/Basket'

const mapStateToProps = ({Basket: {list}}) => ({data: list})

const mapDispatchToProps = dispatch => {
    return {
        increase: (id, note) => dispatch(increase(id, note)),
        decrease: (id, note) => dispatch(decrease(id, note)),
        deleteItem: (id, note) => dispatch(deleteItem(id, note))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Basket);