import {connect} from 'react-redux'
import OrderHistory from '../Component/OrderHistory';

const mapStateToProps = ({Authenticate: {token}}) => ({token});


export default connect(mapStateToProps)(OrderHistory);