import {connect} from 'react-redux'
import ChangePassword from '../Component/ChangePassword';
import { logout } from '../Action/Authenticate';

const mapStateToProps = ({Authenticate: {token, password}}) => ({
  token,
  password
})

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(logout())
})


export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword);