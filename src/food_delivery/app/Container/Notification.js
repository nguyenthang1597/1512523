import {connect} from 'react-redux';
import Notification from '../Component/Notification';
import {fetchNoti} from '../Action/Notification'
const mapStateToProps = ({Authenticate: {token}, Notification: {loading, list}}) => ({
  loading,
  list,
  token
})

const mapDispatchToProps = dispatch => ({
  fetchNoti: (token) => dispatch(fetchNoti(token))
})

export default connect(mapStateToProps, mapDispatchToProps)(Notification);