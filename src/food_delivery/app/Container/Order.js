import {connect} from 'react-redux'
import Order from '../Component/Order'
import {clear} from '../Action/Basket'
const mapStateToProps = ({Basket: {list, idRes}, Authenticate: {token}}) => ({list, idRes, token});
const mapDispatchToProps = dispatch => ({
  clear: () => dispatch(clear())
})
export default connect(mapStateToProps, mapDispatchToProps)(Order)

