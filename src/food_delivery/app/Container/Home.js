import Home from '../Component/Home';
import {connect} from 'react-redux'
import { loadMerchants, loadmoreMerchants, refreshMerchants } from '../Action/Merchant';
import {loadCategory} from '../Action/Category'
import {getDistricts} from '../Action/Address'
import {getInfo} from '../Action/Info';
const mapStateToProps = ({Merchant: {merchants, page, nextPage, hasError, isLoading, loadmore, refresh}, Category: {categories}, Authenticate: {token}}) => ({
  merchants, page, nextPage, hasError, isLoading, loadmore, refresh, token, categories
})

const mapDispatchToProps = dispatch => ({
  loadMerchants: (page,perpage) => dispatch(loadMerchants(page,perpage)),
  loadMore: (page, perpage) => dispatch(loadmoreMerchants(page, perpage)),
  refreshMerchants: (page, perpage) => dispatch(refreshMerchants(page, perpage)),
  loadCate: () => dispatch(loadCategory()),
  getDistricts: () => dispatch(getDistricts()),
  getInfo: (token) => dispatch(getInfo(token))
})

export default connect(mapStateToProps, mapDispatchToProps)(Home);