import {connect} from 'react-redux';
import Profile from '../Component/Profile';
import {getInfo, setInfo} from '../Action/Info'

const mapStateToProps = ({Authenticate: {token}, Info, Address}) => ({
  token,
  Info,
  Address
})

const mapDispatchToProps = dispatch => ({
  getInfo: (token) => dispatch(getInfo(token)),
  setInfo: (value) => dispatch(setInfo(value))
})


export default connect(mapStateToProps, mapDispatchToProps)(Profile)