import {connect} from 'react-redux'
import Comments from '../Component/Comments';

const mapStateToProps = ({Info: {email, userName}}) => ({email, userName});


export default connect(mapStateToProps)(Comments);