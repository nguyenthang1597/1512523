import {connect} from 'react-redux';
import Login from '../Component/Login';
import {authenticate, closeDialog} from '../Action/Authenticate'
const mapDispatchtoProps = dispatch => ({
    authenticate: (info) => dispatch(authenticate(info)),
    closeDialog: () => dispatch(closeDialog())
})

const mapStateToProps = ({Authenticate: {isAuthenticated, hasError, isAuthenticating}}) => ({
    isAuthenticated,
    hasError,
    isAuthenticating
})

export default connect(mapStateToProps, mapDispatchtoProps)(Login)