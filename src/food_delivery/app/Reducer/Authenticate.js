import { AUTH_ERROR, AUTH_REQUEST, AUTH_SUCCESS, CLOSE_DIALOG, LOGOUT } from '../Action/Authenticate'

const initState = {
  isAuthenticating: false,
  isAuthenticated: false,
  token: null,
  hasError: false,
  password: ''
}


export default (state = initState, action) => {
  console.log(action)
  switch (action.type) {
    case AUTH_REQUEST:
      return {
        ...state,
        isAuthenticating: true,
        hasError: false
      }
    case AUTH_SUCCESS:
      return {
        ...state,
        isAuthenticating: false,
        isAuthenticated: true,
        token: action.data.token,
        password: action.data.password
      }

    case AUTH_ERROR:
      return {
        ...state,
        isAuthenticating: false,
        isAuthenticated: false,
        token: null,
        hasError: true
      }
    case CLOSE_DIALOG:
      return {
        ...state,
        hasError: false
      }
    case LOGOUT:
      return {
        ...initState
      }
    default:
      return state;
  }
}


