import {REQUEST_INFO, RECEIVE_INFO, RECEIVE_INFO_FAILER, SET_INFO} from '../Action/Info';

const initState = {
  email: '',
  phone: '',
  address: {
    id: null,
    idDistrict: null,
    idWard: null,
    street: '',
    number: ''
  },
  userName:'',
  avatarUrl: '',
  loading: false,
  hasError: false,
}

export default (state = initState, action) => {
  switch(action.type){
    case REQUEST_INFO:
      return {...state, loading: true}
    case RECEIVE_INFO:
      return {...state, ...action.info, loading: false}
    case RECEIVE_INFO_FAILER:
      return {...initState, hasError: true}
    case SET_INFO:
      var newState;
      if(action.value.userName || action.value.avatarUrl){
        newState = {...state, ...action.value}
      }
      else newState = {...state, address: {...state.address,...action.value}}
      console.log(newState);
      return newState;
    default:
    return state;
  }
}