import {RECEIVE_WARD, RECEIVE_DISTRICT} from '../Action/Address'
const initState = {
  districts: [],
  wards: []
}

export default (state = initState, action) => {
  switch(action.type){
    case RECEIVE_DISTRICT:
      return {...state, districts: action.districts}
    case RECEIVE_WARD:
      return {...state, wards: action.ward}
    default:
    return state;
  }
}