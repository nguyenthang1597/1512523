import { ADD_ITEM, DELETE_ITEM, INCREASE, DECREASE, CLEAR } from '../Action/Basket'

const initState = {
  list: [],
  idRes: null
}

const Basket = (state = initState, action) => {
  switch (action.type) {
    case ADD_ITEM:{
      console.log(action)
      const list = state.list ;
      let item = list.find(i => i.id === action.item.id && i.note === action.item.note)
      if(item)
      {
        return {
          ...state,
          list : state.list.map(i => i.id === item.id && i.note === item.note ? {...i, count: i.count + 1} : i)
        }
      }
        
      else
        return {
          ...state,
          list:  [...state.list, action.item],
          idRes: action.resId
        }
    }

    case DELETE_ITEM:
      let newList = state.list.filter(item => {
        if(item.id === action.id && item.note !== action.note)
          return true;
        if(item.id !== action.id) return true;
        return false;
      })
      if(newList.length)
        return {...state, list: [...newList]}
      else{
        return {...initState}
      }
    case INCREASE:
      let _newList = state.list.map(item => item.id === action.id && item.note === action.note ? {...item, count: item.count + 1 }: item)
      return {
        ...state,
        list: _newList
      }
    case DECREASE:
      return {
        ...state,
        list: state.list.map(item => item.id === action.id && item.note === action.note ? { ...item, count: item.count - 1 <= 0 ? 0 : item.count - 1 } : item )
      }
    case CLEAR:
      return {...initState}
    default:
      return state;
  }
}

export default Basket;