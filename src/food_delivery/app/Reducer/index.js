import {combineReducers} from 'redux'
import Authenticate from './Authenticate'
import Merchant from './Merchant'
import Category from './Category'
import Basket from './Basket'
import Address from './Address'
import Info from './Info'
import Notification from './Notification'
import Setting from './Setting'
export default combineReducers({
  Authenticate,
  Merchant,
  Category,
  Basket,
  Address,
  Info,
  Notification,
  Setting
})
