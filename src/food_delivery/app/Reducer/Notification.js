import {REQUEST_NOTI, RECEIVE_NOTI} from '../Action/Notification';

const initState = {
  loading: false,
  list: []
}

export default (state = initState, action) => {
  switch(action.type){
    case REQUEST_NOTI:
      return {...state, loading: true};
    case RECEIVE_NOTI:
      return {...state, list: action.noti, loading: false}
    default:
      return state;
  }
}