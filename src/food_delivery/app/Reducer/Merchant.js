import { ERROR, LOADMORE, RECEIVE, REQUEST, REFRESH } from '../Action/Merchant';

const initState = {
  merchants: [],
  page: 1,
  nextPage: null,
  hasError: false,
  isLoading: false,
  loadmore: false,
  refresh: false
}

const merchants = (state = initState, action) => {
  switch (action.type) {
    
    case REQUEST:
      return {
        ...state, isLoading: true
      }
    case RECEIVE:
      return {
        ...state, isLoading: false, hasError: false, nextPage: action.data.next_page, merchants: state.merchants.concat(action.data.data), loadmore: false, refresh: false
      }
    case LOADMORE:
      return {
        ...state, loadmore: true
      }
    case ERROR:
      return {
        ...state, hasError: true
      }
    case REFRESH:
      return {
        merchants: [],
        page: 1,
        nextPage: null,
        hasError: false,
        isLoading: false,
        loadmore: false,
        refresh: true
      }
    default: return state;
  }
}

export default merchants;