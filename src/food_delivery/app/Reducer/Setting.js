import {CHANGE_LANGUAGE, CHANGE_NOTIFICATION} from '../Action/Setting'
const InitState = {
  language: 'en',
  notification: false
}


const Setting = (state = InitState, action) => {
  switch(action.type){
    case CHANGE_LANGUAGE:
      return {...state, language: action.language}
    case CHANGE_NOTIFICATION: {
      return {...state, notification: !state.notification}
    }
    default:
      return state;
  }
}

export default Setting;