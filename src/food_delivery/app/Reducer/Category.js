import { CATEGORY_ERROR, CATEGORY_RECEIVE } from '../Action/Category';

const initState = {
  error: false,
  categories: []
}

export default category = (state = initState, action) => {
  switch (action.type) {
    case CATEGORY_ERROR:
      return { ...state, error: true };
    case CATEGORY_RECEIVE:
      return { ...state, categories: action.data }
    default:
      return state;
  }
}