import {API_URL} from '../Config';
import axios from 'axios';


export const fnGetInfo = (token) => axios.get(`${API_URL}/getInfo`, {
  headers: {
    "Authorization": `Bearer ${token}`
  }
});

export const fnUpdateInfo = async (token, info) => {
  let INFO = {
    phone: info.phone,
    idDistrict: info.address.idDistrict,
    idWard: info.address.idWard,
    street: info.address.street,
    userName: info.userName
  }
  console.log(INFO);
return  axios.post(`${API_URL}/updateInfo`, INFO, {
   headers: {
     "Authorization": `Bearer ${token}`,
     "Content-Type": "application/json"
   }
  })
}

export const fnUpdateAvatar = async  (token, uri, type, name) => {
  let data = new FormData();
  data.append('file', {
  	uri: uri,
  	type: type,
  	name: name
  })
  const config = {
     headers: {
       "Authorization": `Bearer ${token}`,
       "Content-Type": "application/json"
     }
    }
  let url = `${API_URL}/updateAvatar`;
  console.log(url);
  return axios.put(url, data, config)
}