import {API_URL} from '../Config'
import axios from 'axios';

export const fnLogin = (info) => axios.post(`${API_URL}/login`, info);
export const fnChangePassword = (token, newPassword) => axios.post(`${API_URL}/updatePassword`, {password: newPassword}, {
  headers: {
    "Authorization": `Bearer ${token}`
  }
})
export const fnSignup = (data) => axios.post(`${API_URL}/register`, data)
export const fnForgetPassword = data => axios.post(`${API_URL}/forgetPassword`,data)