import {API_URL} from '../Config';
import axios from 'axios';

export const fnLoadMerchants = (page, perpage) => axios.get(`${API_URL}/restaurant/getAll/${perpage}&${page}`);
export const fnLoadMerchatnsByCategory = (id) => axios.get(`${API_URL}/restaurant/getCategory/${id}`);
export const fnGetMerchantById = (id) => axios.get(`${API_URL}/restaurant/getMenu/${id}`);
export const fnGetComment = (id) => axios.get(`${API_URL}/restaurant/getAllCommentById/${id}`);
export const fnSendComment = (idRestaurant, name, content) => axios.post(`${API_URL}/restaurant/addComment`, {
  idRestaurant,
  name,
  content
})