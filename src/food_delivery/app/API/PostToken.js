import {API_URL} from '../Config'
import axios from 'axios';


export const fnPostToken = (token, deviceId, platform) => {
  return axios.post(`${API_URL}/deviceToken`, {
    deviceId,
    platform
  }, {
    headers: {
      "Authorization": `Bearer ${token}`
    }
  })
}