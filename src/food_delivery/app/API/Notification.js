import {API_URL} from '../Config';
import axios from 'axios';


export const fnGetNoti = (token) => axios.get(`${API_URL}/getNoti`, {
  headers: {
    "Authorization": `Bearer ${token}`
  }
});