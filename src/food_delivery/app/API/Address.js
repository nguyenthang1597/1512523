import {API_URL} from '../Config';
import axios from 'axios';

export const getDistrict = () => axios.get(`${API_URL}/district/getAll`);

export const getWards = (id) => axios.get(`${API_URL}/ward/getAllByDistrict?id=${id}`)