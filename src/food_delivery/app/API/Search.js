import {API_URL} from '../Config'
import axios from 'axios';


export const food = (text) => axios.get(`${API_URL}/food/searchfood?foodname=${text}`)

export const restaurant = text => axios.get(`${API_URL}/restaurant/search?name=${text}`)