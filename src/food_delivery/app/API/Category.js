import {API_URL} from '../Config'
import axios from 'axios';

export const fnGetAll = () => axios.get(`${API_URL}/categories/getAll`);