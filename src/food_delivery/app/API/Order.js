import {API_URL} from '../Config';
import axios from 'axios';

export const Create = (token, data) => axios.post(`${API_URL}/order/create/v2`, data, {
  headers: {
    "Authorization": `Bearer ${token}`
  }
})

export const GetHistory = (token) => axios.get(`${API_URL}/order/getAll`, {
  headers: {
    "Authorization": `Bearer ${token}`
  }
})

export const GetOrder = (token, id) => axios.get(`${API_URL}/order/getOrder/${id}`, {
  headers: {
    "Authorization": `Bearer ${token}`
  }
})

export const GetFood = id => axios.get(`${API_URL}/food/${id}`)