import * as en from './en';
import * as vi from './vi'
import I18n from 'react-native-i18n';

I18n.fallbacks = true;

I18n.translations = {
  en,
  vi
}


export default I18n;
