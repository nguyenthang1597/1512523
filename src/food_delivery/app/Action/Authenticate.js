import {fnLogin} from '../API/Account'
import {Actions} from 'react-native-router-flux'

export const AUTH_REQUEST = 'AUTH_REQUEST'
export const AUTH_SUCCESS = 'AUTH_SUCCESS'
export const AUTH_ERROR = 'AUTH_ERROR';
export const CLOSE_DIALOG = 'CLOSE_DIALOG'
export const LOGOUT = 'LOGOUT'
const reqAuth = () => ({ type: AUTH_REQUEST })
const reqSuccess = data => ({type: AUTH_SUCCESS,data})
const error = () => ({type: AUTH_ERROR})
export const closeDialog = () => ({type: CLOSE_DIALOG})
export const authenticate = info => dispatch => {
  dispatch(reqAuth());

  return fnLogin(info)
  .then(res => {
    dispatch(reqSuccess({token: res.data.token, password: info.password}));
    Actions.Main({type: 'reset'})
  })
  .catch(e => {
    dispatch(error())
  })
}
export const logout = () => ({type: LOGOUT})