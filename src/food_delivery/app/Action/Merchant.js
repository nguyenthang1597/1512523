import {fnLoadMerchants} from '../API/Merchant'
export const REQUEST = 'REQUEST';
export const RECEIVE = 'RECEIVE';
export const ERROR = 'ERROR';
export const LOADMORE = 'LOADMORE';
export const REFRESH = 'REFRESH';
export function requestMerchants() {
  return {
    type: REQUEST
  }
}

function loadMore(){
  return {
    type: LOADMORE
  }
}

function receiveMerchants(data){
  return {
    type: RECEIVE,
    data
  }
}

export function hasError(){
  return {
    type: ERROR
  }
}

export function loadMerchants(page, perpage){
  return dispatch => {
    dispatch(requestMerchants());
    return fnLoadMerchants(page,perpage)
    .then(res => dispatch(receiveMerchants(res.data)))
    .catch(err => dispatch(hasError()))
  }
}

export function loadmoreMerchants(page, perpage){
  return dispatch => {
    dispatch(loadMore());
    return fnLoadMerchants(page,perpage)
    .then(res => dispatch(receiveMerchants(res.data)))
    .catch(err => dispatch(hasError()))
  }
}

function makeRefresh(){
  return{
    type: REFRESH
  }
}

export function refreshMerchants(page,perpage){
  return dispatch => {
    dispatch(makeRefresh());
    return fnLoadMerchants(page,perpage)
    .then(res => dispatch(receiveMerchants(res.data)))
    .catch(err => dispatch(hasError()))
  }
}