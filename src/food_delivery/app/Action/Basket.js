export const ADD_ITEM = 'ADD_TO_CART';
export const DELETE_ITEM = 'DELETE_ITEM';
export const INCREASE = 'INCREASE';
export const DECREASE = 'DECREASE';
export const CLEAR = 'CLEAR';
export const addItem = (item, resId) => {
  return {
    type: ADD_ITEM,
    item,
    resId
  }
}

export const clear = () => ({type: CLEAR})

export const deleteItem = (id, note) => {
  return {
    type: DELETE_ITEM,
    id,
    note
  }
}

export const increase = (id, note) => {
  return {
    type: INCREASE,
    id,
    note
  }
}

export const decrease = (id, note) => {
  return {
    type: DECREASE,
    id,
    note
  }
}
