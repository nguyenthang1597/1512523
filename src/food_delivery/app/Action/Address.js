import {getDistrict, getWards} from '../API/Address'
export const RECEIVE_DISTRICT = 'RECEIVE_DISTRICT';
export const RECEIVE_WARD = 'RECEIVE_WARD'


const receiveDistrict = (districts) => ({type: RECEIVE_DISTRICT, districts});
export const getDistricts = () => dispatch => {
  let districts = [];
  return getDistrict()
  .then(res => {
    dispatch(receiveDistrict(res.data))
    districts = res.data;
    console.log(districts);
    let arr = districts.map(i => getWards(i.id));
    return Promise.all(arr)
  })
  .then(res => {
    let obj = {};

    let arr = res.map(i => i.data);

    districts.forEach((e, index) => {
      obj[e.id] = arr[index]
    })
    dispatch(receiveWard(obj))
  })
  .catch(e => dispatch(receiveDistrict([])))
}

const receiveWard = (ward) => ({type: RECEIVE_WARD, ward})