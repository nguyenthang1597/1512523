import {fnGetInfo} from '../API/Info'
export const REQUEST_INFO = 'REQUEST_INFO'
export const RECEIVE_INFO = 'RECEIVE_INFO';
export const RECEIVE_INFO_FAILER = 'RECEIVE_INFO_FAILER';
export const SET_INFO = 'SET_INFO';
const makeRequestInfo = () => ({type: REQUEST_INFO});
const receiveInfo = info => ({type: RECEIVE_INFO, info});
const receiveFailer = () => ({type: RECEIVE_INFO_FAILER});
const _setInfo = value => ({type: SET_INFO, value})

export const getInfo = (token) => dispatch => {
  dispatch(makeRequestInfo());
  return fnGetInfo(token)
  .then(res => {
    console.log(res.data);
    dispatch(receiveInfo(res.data))
  })
  .catch(e => {
    console.log(e);
    dispatch(receiveFailer())
  })
}


export const setInfo = (value) => dispatch => {
  dispatch(_setInfo(value));
}

export const updateInfo = (name, value) => dispatch