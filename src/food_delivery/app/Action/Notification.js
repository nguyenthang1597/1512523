import {fnGetNoti} from '../API/Notification'

export const REQUEST_NOTI = 'REQUEST_NOTI';
export const RECEIVE_NOTI = 'RECEIVE_NOTI';

const requestNoti = () => ({type: REQUEST_NOTI});
const receiveNoti = (noti) => ({type: RECEIVE_NOTI, noti});


export const fetchNoti = (token) => dispatch => {
  dispatch(requestNoti());
  return fnGetNoti(token)
  .then(res => {
    console.log(res);
    dispatch(receiveNoti(res.data))
  })
  .catch(e => dispatch(receiveNoti([])))
}