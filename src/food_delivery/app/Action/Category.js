import {fnGetAll} from '../API/Category'
export const CATEGORY_RECEIVE = 'CATEGORY_RECEIVE';
export const CATEGORY_ERROR = 'CATEGORY_ERROR';


const receiveCATEGORY = (data) => {
  return {
    type: CATEGORY_RECEIVE,
    data
  }
}

const receiveErrorCategory = () => {
  return {
    type: CATEGORY_ERROR
  }
}

export const loadCategory = () => dispatch => {
    return fnGetAll()
      .then(res => dispatch(receiveCATEGORY(res.data)))
      .catch(err => dispatch(receiveErrorCategory()))
  }