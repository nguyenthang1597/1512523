export const CHANGE_LANGUAGE = 'CHANGE_LANGUAGE';
export const CHANGE_NOTIFICATION = 'CHANGE_NOTIFICATION';




export const changeLanguage = language => ({type: CHANGE_LANGUAGE, language})
export const changeNotification = () => ({type: CHANGE_NOTIFICATION})