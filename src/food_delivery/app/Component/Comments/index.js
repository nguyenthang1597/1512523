import React, { Component } from 'react'
import { View, StyleSheet, FlatList, TextInput, TouchableOpacity, Text, Dimensions, Alert } from 'react-native'
import _ from 'lodash';
import { fnGetComment, fnSendComment } from '../../API/Merchant';
import Comment from './Comment'
import I18n from '../../I18n'
export default class Comments extends Component {
  state = {
    page: 0,
    list: [],
    comments: [],
    content: ''
  }

  componentDidMount = async () => {
    await this.getComment();
  };

  getComment = async () => {
    try {
      let res = await fnGetComment(this.props.id);
      let _list = res.data.result;
      _list = _.chunk(_list, 10);
      this.setState({
        list: _list,
        comments: [..._list[0]],
        page: 0,
        content: ''
      })
    } catch (error) {
      this.setState({
        list: [],
        page: 0,
        comments: [],
        content: ''
      })
    }
  }
  _onEndReached = () => {
    const { page, list, comments } = this.state;
    if (page + 1 >= list.length)
      return;
    let _comments = [...comments, ...list[page + 1]];
    this.setState({
      page: page + 1,
      comments: _comments
    })
  }

  submit = () => {
    let name = this.props.userName === '' ? this.propsp.email : this.props.userName;
    return fnSendComment(this.props.id, name, this.state.content)
    .then(async res => {
      await this.getComment();
    })
    .catch(e => {
      Alert.alert(I18n.t('cmtError'), I18n.t('cmtErrorMsg'));
      this.setState({
        content: ''
      })
    })
  }
  render() {
    return (
      <View style={Style.container}>
        <View style={{ flex: 1 }}>
          <FlatList
            data={this.state.comments}
            renderItem={({ item }) => <Comment {...item} />}
            keyExtractor={(item, index) => index.toString()}
            onEndReached={this._onEndReached}
            onEndReachedThreshold={0.001}
          />
        </View>
        <View style={Style.form}>
          <TextInput multiline style={Style.input} onChangeText={text => this.setState({content: text})} value={this.state.content}/>
          <TouchableOpacity style={Style.send} disabled={this.state.content === ''} onPress={() => this.submit()}>
            <Text style={Style.sendText}>{I18n.t('btnsendcmt')}</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const Style = StyleSheet.create({
  container: {
    flex: 1
  },
  form: {
    flexDirection: 'row',
    alignItems: 'center',
    position: 'relative',
    width: '100%',
    height: 80,
    elevation: 3,
    borderTopWidth: 1,
  },
  input: {
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    height: 80,
    color: '#000',
    width: '100%',
    paddingRight: 85,
  },
  send: {
    width: 80,
    height: 78,
    backgroundColor: '#4caf50',
    position: 'absolute',
    right: 0,
    borderTopLeftRadius: 39,
    borderBottomLeftRadius: 39
  },
  sendText: {
    lineHeight: 80,
    textAlign: 'center',
    fontSize: 24,
    color: 'white',
    fontWeight: '500'
  }
})