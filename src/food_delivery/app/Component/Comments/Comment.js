import React from 'react';
import {View, Text} from 'react-native'
import moment from 'moment'
const Comment = ({name, content, createAt}) => {
  return (
    <View style={{width: '100%', flexDirection: 'column', minHeight: 60, marginVertical: 2, backgroundColor: 'white'}}>
      <View style={{height: 20, flexDirection: 'row', width: '100%'}}>
        <View style={{flex: 1, overflow: 'scroll'}}>
          <Text style={{fontSize: 18, fontWeight: '500', paddingLeft: 5}} numberOfLines={1}>{name}</Text>
        </View>
        <View style={{flex: 1}}>
          <Text style={{textAlign: 'right', paddingRight: 5}}>{moment(createAt).format('DD/MM/YYYY - HH:mm:ss')}</Text>
        </View>
      </View>
      <View style={{width: '100%', paddingLeft: 5}}>
        <Text>{content}</Text>
      </View>
    </View>
  );
};

export default Comment;