import React, { Component } from 'react'
import { View, StyleSheet, TextInput, TouchableOpacity, FlatList, ScrollView, Text } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import { food, restaurant } from '../../API/Search'
import ItemList from './SearchItemList'
import I18n from '../../I18n'
export default class Search extends Component {
  state = {
    search: '',
    food: [],
    restaurant: []
  }

  handleSubmit = () => {
    this.setState({
      food: [],
      restaurant: []
    }, () => {
      const { search } = this.state;
      Promise.all([food(search), restaurant(search)])
        .then(res => {
          this.setState({
            food: res[0].data,
            restaurant: res[1].data
          })
        })
    })
  }
  render() {
    return (
      <View style={Style.container}>
        <View style={Style.inputWrapper}>
          <TextInput style={Style.input} placeholder={I18n.t('search')} onChangeText={text => this.setState({ search: text })} />
          <TouchableOpacity style={Style.btnEye} disabled={this.state.search.length === 0} onPress={() => this.handleSubmit()}>
            <Icon name='search' size={22} style={{ lineHeight: 40, textAlign: 'center' }} />
          </TouchableOpacity>
        </View>
        <ScrollView>
          {
            this.state.restaurant.length > 0 &&
            <View style={{width: '100%', height: 30, backgroundColor: 'white', paddingLeft: 5}}>
              <Text style={{fontSize: 22, color: 'orange'}}>{I18n.t('restaurant')}</Text>
            </View>
          }
          <FlatList
            numColumns={2}
            data={this.state.restaurant || []}
            renderItem={({ item, index }) => <ItemList type={1} key={index} index={index} item={item} />}
            keyExtractor={(item, index) => index.toString()}
          />
          {
            this.state.food.length > 0 &&
            <View style={{width: '100%', height: 30, backgroundColor: 'white', paddingLeft: 5}}>
              <Text style={{fontSize: 22, color: 'orange'}}>{I18n.t('food')}</Text>
            </View>
          }
          <FlatList
            numColumns={2}
            data={this.state.food || []}
            renderItem={({ item, index }) => <ItemList type={2} key={index} index={index} item={item} />}
            keyExtractor={(item, index) => index.toString()}
          />
        </ScrollView>
      </View>
    )
  }
}

const Style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#cecece'
  },
  form: {
    width: '100%',
    height: 200,
    position: 'absolute',
    top: '50%',
    transform: [{
      translateY: -100
    }],
  },
  input: {
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    height: 40,
    marginHorizontal: 10,
    paddingLeft: 15,
    borderRadius: 20,
    color: '#000',
    paddingRight: 65
  },
  inputWrapper: {
    width: '90%',
    height: 40,
    position: 'relative',
    marginTop: 5,
    marginBottom: 5,
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  btnEye: {
    position: 'absolute',
    right: 10,
    width: 60,
    backgroundColor: '#4caf50',
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
  }
})