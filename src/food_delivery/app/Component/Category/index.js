import React from 'react'
import {View, FlatList, ActivityIndicator, RefreshControl, StyleSheet} from 'react-native'
import ItemList from '../MerchantItem'
import {fnLoadMerchatnsByCategory} from '../../API/Merchant'
class Category extends React.Component {
  state = {
    loading: false,
    refresh: false,
    data: []
  }
  componentDidMount() {
    this.setState({loading: true}, () => {
      return fnLoadMerchatnsByCategory(this.props.id)
      .then(res => this.setState({loading: false, data: res.data}))
      .catch(e => this.setState({loading: false}))
    })
  }
  render(){
    const {loading, refresh, data} = this.state;
    return (
      <View style={style.container}>
        <View style={style.body}>
          {
            loading ? <ActivityIndicator size={60} /> :
            <FlatList contentContainerStyle={style.list}
              refreshControl={
                <RefreshControl
                refreshing={refresh}
                onRefresh={() => refreshMerchants(1,10)}
                />
              }
              scrollEventThrottle={60}
              onEndReached={this._onEndReached}
              onEndReachedThreshold={0.1}
              numColumns={2}
              data={data}
              renderItem={({ item, index }) => <ItemList key={index} index={index} item={item} />}
              keyExtractor={( item, index) => index.toString()}
            />
        }
        </View>
      </View>
    )
  }
}

const style = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#E1E2E1'
	},

	body: {
		flex: 1,
		justifyContent: "center",
		position:'relative'
	},
	list: {
		flexDirection: 'column',
		position: 'relative',
    backgroundColor: '#E1E2E1'
	},
	item: {
		backgroundColor: 'red',
		margin: 3,
		width: 100,
	}
})
export default Category;
