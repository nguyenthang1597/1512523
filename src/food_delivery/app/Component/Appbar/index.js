import React from 'react';
import { View, StyleSheet, StatusBar, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux';
export default (props) => {
    return (
        <View style={style.appBar}>
            <StatusBar backgroundColor='#ffa726' />
            {props.back &&
                <TouchableOpacity onPress={() => Actions.pop()}>
                    <Icon name='chevron-left' size={26} color='black' style={{ lineHeight: 50 }} />
                </TouchableOpacity>}
            <Text style={{ flex: 1, textAlign: 'center', lineHeight: 50, color: 'black', fontSize: 30, alignItems: 'center' }}>{props.title}</Text>
            {
                props.map &&
                <TouchableOpacity onPress={() => Actions.push('map')} style={style.map}>
                    <Icon name='map' size={30} color="black" style={{ lineHeight: 50 }} />
                </TouchableOpacity>
            }
        </View>
    );
}
const style = StyleSheet.create({
    appBar: {
        backgroundColor: '#ffa726',
        height: 50,
        flexDirection: 'row',
    },
    map: {
        position: 'absolute',
        right: 5
    }
})