import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'
import moment from 'moment'
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux';

const ListItem = ({ totalPrice, address, date, id, phone }) => {
  return (
    <TouchableOpacity style={Style.container} onPress={() => Actions.orderdetail({id, phone, date, totalPrice, address})}>
      <View style={{ flexDirection: 'row' }}>
        <Icon name='map-marker' size={22} style={{ paddingLeft: 5, lineHeight: 30 }} />
        <Text style={{ fontSize: 20, lineHeight: 30, paddingLeft: 10 }}>{address}</Text>
      </View>
      <View style={{flexDirection: 'row'}}>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <Icon name='calendar' size={18} style={{paddingLeft: 5}}/>
          <Text style={{paddingLeft: 5}}>{moment(date).format('DD/MM/YYYY - HH:mm:ss')}</Text>
        </View>
        <View style={{flex: 1, flexDirection: 'row'}}>
          <Icon name='credit-card' size={18} style={{paddingLeft: 5}}/>
          <Text style={{paddingLeft: 10}}>{totalPrice} VND</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const Style = StyleSheet.create({
  container: {
    width: '100%',
    height: 80,
    flexDirection: 'column',
    backgroundColor: 'white',
    marginVertical: 5,
  }
})

export default ListItem;