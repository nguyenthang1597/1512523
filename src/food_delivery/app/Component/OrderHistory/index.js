import React, { Component } from 'react'
import {View, StyleSheet, ActivityIndicator} from 'react-native'
import { GetHistory } from '../../API/Order';
import ListItem from './ListItem';
export default class OrderHistory extends Component {
  state = {
    list: [],
    loading: true
  }
  getHistory = (token) => {
    return GetHistory(token)
    .then(res => this.setState({loading: false, list: res.data}))
    .catch(res => this.setState({loading: false, list: []}))
  }

  componentDidMount(){
    return this.getHistory(this.props.token);
  }


  render() {
    return (
      <View style={Style.container}>
        {
          this.state.loading && 
          <View style={{flex:1, alignItems: 'center', justifyContent: 'center'}}>
            <ActivityIndicator size={32}/>
          </View>
        }
        {
          this.state.list.map((e, index) => <ListItem key={index} {...e}/>)
        }
      </View>
    )
  }
}

const Style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E1E2E1',
  }
})