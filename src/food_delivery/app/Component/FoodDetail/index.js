import React from 'react'
import { View, Text, Image, TouchableOpacity, TextInput, Alert } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import I18n from '../../I18n'
import { Actions } from 'react-native-router-flux';
class FoodDetail extends React.Component {
  state = {
    note: '',
    count: 1
  }
  render() {
    const { id, fname, fprice, fimage, addItem, idRes, resId} = this.props;
    console.log(this.props)
    const { count, note } = this.state;
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <View style={{ width: '100%', height: 180 }}>
          <Image source={{ uri: fimage }} style={{ width: '100%', height: 180 }} />
        </View>
        <View style={{ flexDirection: 'column' }}>
          <View style={{ height: 80 }}>
            <Text style={{ lineHeight: 30, fontWeight: '600', fontSize: 24, textAlign: 'center' }}>{fname}</Text>
          </View>
          <View style={{ flexDirection: 'row', marginTop: 10, height: 40, alignItems: 'center' }}>
            <Text style={{ fontSize: 20, paddingLeft: 10, lineHeight: 40 }}>Số lượng: </Text>
            <TouchableOpacity onPress={() => this.setState({ count: count - 1 > 1 ? count - 1 : 1 })}>
              <Icon name='minus-circle' size={24} style={{ lineHeight: 40 }} />
            </TouchableOpacity>
            <Text style={{ paddingHorizontal: 20 }}>{count}</Text>
            <TouchableOpacity onPress={() => this.setState({ count: count + 1 })}>
              <Icon name='plus-circle' size={24} style={{ lineHeight: 40 }} />
            </TouchableOpacity>
          </View>
          <Text style={{ fontSize: 20, paddingLeft: 10 }}>Note</Text>
          <TextInput style={{ flex: 1, backgroundColor: '#E1E1E1', minHeight: 180 }} multiline onChangeText={text => this.setState({ note: text })} />
          <TouchableOpacity style={{ backgroundColor: '#4caf50', height: 60, borderRadius: 20, marginLeft: 'auto', marginRight: 'auto', position: 'relative', width: '50%'}}
            onPress={() => {
              if(!idRes || idRes === resId ){
                addItem({id, name:fname, image: fimage, price: fprice, count, note}, resId)
                Actions.Main();
                Actions.basket();
              }
              else{
                Alert.alert('Thông báo', 'Vui lòng hoàn thành đơn hàng trước khi thêm món ăn mới',
                [
                  {text: 'Hủy', style: 'cancel'},
                  {text: 'Đi đến giỏ hàng', onPress: () => Actions.basket()}
                ])
              }
            }}
          >
            <Text style={{ lineHeight: 60, textAlign: 'center', fontSize: 20, color: 'white' }}>{I18n.t('addToCart')}</Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

export default FoodDetail;