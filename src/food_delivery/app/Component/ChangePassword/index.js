import React, { Component } from 'react'
import { View, Text, StyleSheet, TextInput, TouchableOpacity, Alert, ActivityIndicator } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import {fnChangePassword} from '../../API/Account'
import {Actions} from 'react-native-router-flux'
import I18n from '../../I18n'
export default class ChangePassword extends Component {
  state = {
    showPassword: false,
    showConfirmPassword: false,
    password: '',
    confirmPassword: '',
    currentPassword: '',
    loading: false
  }

  toggle = (name) => {
    let state = {...this.state};
    state[name] = !state[name];
    this.setState(state);
  }

  setValue = (name, value) => {
    let state = {...this.state};
    state[name] = value;
    this.setState(state)
  }

  handleSubmit = () => {
    const {currentPassword, password, confirmPassword} = this.state;
    this.setState({loading: true}, () => {
      if(currentPassword !== this.props.password){
        Alert.alert(I18n.t('failed'), I18n.t('notcorrect'));
        this.setState({loading: false})
        return;
      }
      if(password !== confirmPassword){
        Alert.alert(I18n.t('failed'), I18n.t('notmatch'));
        this.setState({loading: false})
        return
      }
      return fnChangePassword(this.props.token, password)
      .then(res => {
        this.props.logout();
        Alert.alert(I18n.t('success'), I18n.t('successmessage'))
        setTimeout(() => Actions.login({type: 'reset'}), 1000 );
        
      })
      .catch(e => {
        console.log(e)
        Alert.alert(I18n.t('failed'), I18n.t('failedchange'))
        this.setState({loading: false})
      })
    })
  }
  render() {
    const {loading} = this.state;
    return (
      <View style={Style.container}>
        <View style={Style.form}>
          <View style={Style.inputWrapper}>
            <Icon name='lock' size={22} style={Style.inlineImg} />
            <TextInput style={Style.input} placeholder={I18n.t('currentpassword')} onChangeText={text => this.setValue('currentPassword', text)} />
          </View>
          <View style={Style.inputWrapper}>
            <Icon name='lock' size={22} style={Style.inlineImg} />
            <TextInput style={Style.input} secureTextEntry={!this.state.showPassword} placeholder={I18n.t('newpassword')} onChangeText={text => this.setValue('password', text)}/>
            <TouchableOpacity style={Style.btnEye} onPress={() => this.toggle('showPassword')}>
              <Icon name="eye" size={30} style={{ lineHeight: 40 }} />
            </TouchableOpacity>
          </View>
          <View style={Style.inputWrapper}>
            <Icon name='lock' size={22} style={Style.inlineImg} />
            <TextInput style={Style.input} secureTextEntry={!this.state.showConfirmPassword} placeholder={I18n.t('confirmnewpassword')} onChangeText={text => this.setValue('confirmPassword', text)} />
            <TouchableOpacity style={Style.btnEye} onPress={() => this.toggle('showConfirmPassword')}>
              <Icon name="eye" size={30} style={{ lineHeight: 40 }} />
            </TouchableOpacity>
          </View>
          <TouchableOpacity style={Style.btn} onPress={() => this.handleSubmit()}>
            {loading && <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}><ActivityIndicator size={20} color='white' /></View>}
            {
              !loading && <Text style={Style.loginText}>{I18n.t('changepassword')}</Text>
            }
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}


const Style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#cecece'
  },
  form: {
    width: '100%',
    height: 200,
    position: 'absolute',
    top: '50%',
    transform: [{
      translateY: -100
    }],
  },
  input: {
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    height: 40,
    marginHorizontal: 10,
    paddingLeft: 45,
    borderRadius: 20,
    color: '#000'
  },
  inputWrapper: {
    width: '90%',
    height: 40,
    position: 'relative',
    marginTop: 5,
    marginBottom: 5,
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  btn: {
    width: '50%',
    height: 50,
    position: 'relative',
    marginLeft: 'auto',
    marginRight: 'auto',
    backgroundColor: '#4caf50',
    marginTop: 10,
    borderRadius: 25,
    marginBottom: 0
  },
  loginText: {
    textAlign: 'center',
    fontSize: 18,
    lineHeight: 50,
    color: 'white'
  },
  inlineImg: {
    position: 'absolute',
    width: 22,
    zIndex: 100,
    left: 22,
    lineHeight: 40
  },
  btnEye: {
    position: 'absolute',
    right: 20
  }
})