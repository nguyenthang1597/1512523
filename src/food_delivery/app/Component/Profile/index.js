import React, { Component } from 'react'
import { StyleSheet, View, TextInput, Picker, Text, TouchableOpacity, ScrollView, Alert, ActivityIndicator, Image } from 'react-native'
import I18n from '../../I18n'
import {fnUpdateInfo, fnUpdateAvatar} from '../../API/Info'
import ImagePicker from "react-native-image-picker";
export default class Profile extends Component {
  state = {
    updateLoading: false,
    avatarUrl: null
  }
  handleDistrictChange = (value, index) => {
    this.props.setInfo({idDistrict: value})
  }

  handleWardChange = (value, index) => {
    this.props.setInfo({idWard: value})
  }

  handleSubmit = () => {
    this.setState({updateLoading: true},async () => {
      if(this.state.avatarUrl){
        try {
          let _fnUpdateAvatar = await fnUpdateAvatar(this.props.token, this.state.avatarUrl.uri, this.state.avatarUrl.type, this.state.avatarUrl.fileName);
          let _fnUpdateInfo = await fnUpdateInfo(this.props.token, this.props.Info);
          this.setState({
            updateLoading: false
          })
          this.props.getInfo(this.props.token);
          Alert.alert(I18n.t('updateSuccess'), I18n.t('updateSuccessMessge'))
        } catch (e) {
          console.error(e.request);
          this.setState({
            updateLoading: false
          })
          this.props.getInfo(this.props.token);
          Alert.alert(I18n.t('updateError'), I18n.t('updateErrorMessge'));
        }
      }
      else {
        fnUpdateInfo(this.props.token, this.props.Info)
        .then(res => {
          this.setState({
            updateLoading: false
          })
          Alert.alert(I18n.t('updateSuccess'), I18n.t('updateSuccessMessge'))
        })
        .catch(e => {
          this.setState({
            updateLoading: false
          })
          this.props.getInfo(this.props.token);
          Alert.alert(I18n.t('updateError'), I18n.t('updateErrorMessge'));
        })
      }


    })
  }

  pickImageHandler = () => {
    ImagePicker.showImagePicker({
      title: I18n.t('imagePickerTitle'),
      maxWidth: 800,
      maxHeight: 600,
      cancelButtonTitle: I18n.t('cancelButtonTitle'),
      takePhotoButtonTitle: I18n.t('takePhotoButtonTitle'),
      chooseFromLibraryButtonTitle: I18n.t('chooseFromLibraryButtonTitle')
    }, res => {
      if (res.didCancel) {
        return;
      } else if (res.error) {
        return;
      } else {
        this.setState({
          avatarUrl:res
        })
      }
    });
  }
  render() {
    const {Info, Address: {districts, wards}} = this.props;
    if(typeof districts !== 'undefined' && typeof wards !== 'undefined' && !Info.loading && !Info.hasError){
      const _wards = wards[Info.address.idDistrict] || []
      let src = this.state.avatarUrl || {uri: `https://${Info.avatarUrl}`};
      return (
        <View style={style.container}>
            <ScrollView style={style.content}>
              <TouchableOpacity style={style.avatarView} onPress={() => this.pickImageHandler()}>
                <Image source={src} style={style.avatar}/>
              </TouchableOpacity>
              <View style={style.field}>
                <View style={style.label}>
                  <Text style={{ lineHeight: 50, fontSize: 16 }}>{I18n.t('username')}: </Text>
                </View>
                <TextInput style={style.input} value={Info.userName} onChangeText={(text) => this.props.setInfo({userName: text})} />
              </View>
              <View style={style.field}>
                <View style={style.label}>
                  <Text style={{ lineHeight: 50, fontSize: 16 }}>{I18n.t('phone')}: </Text>
                </View>
                <TextInput style={style.input} value={Info.phone} onChangeText={(text) => this.props.setInfo({phone: text})} />
              </View>
              <View style={style.field}>
                <View style={style.label}>
                  <Text style={{ lineHeight: 50, fontSize: 16 }}>{I18n.t('district')}: </Text>
                </View>
                <Picker style={style.input} onValueChange={this.handleDistrictChange} selectedValue={Info.address.idDistrict}>
                  {
                    districts.map((item, index) => <Picker.Item key={index} label={item.name} value={item.id} />)
                  }
                </Picker>
              </View>
              <View style={style.field}>
                <View style={style.label}>
                  <Text style={{ lineHeight: 50, fontSize: 16 }}>{I18n.t('ward')}: </Text>
                </View>
                <Picker style={style.input} selectedValue={Info.address.idWard} onValueChange={this.handleWardChange}>
                  {
                    _wards.map((item, index) => <Picker.Item key={index} label={item.name} value={item.id} />)
                  }
                </Picker>
              </View>
              <View style={style.field}>
                <View style={style.label}>
                  <Text style={{ lineHeight: 50, fontSize: 16 }}>{I18n.t('street')}: </Text>
                </View>
                <TextInput style={style.input} value={Info.address.street} onChangeText={text => this.props.setInfo({street: text})}/>
              </View>
              <TouchableOpacity style={this.state.updateLoading ? style.buttonLoading : style.button} onPress={() => {if(!this.state.loading) this.handleSubmit()}} >
                {
                  !this.state.updateLoading && <Text style={{ lineHeight: 50, fontSize: 24, fontWeight: '800', textAlign: 'center' }}>{I18n.t('update')}</Text>
                }
                {
                  this.state.updateLoading && <ActivityIndicator size={30} color='white'/>
                }
              </TouchableOpacity>
            </ScrollView>
        </View>
      )
    }

    else
      return (
        <View style={[style.container, {alignItems: 'center', justifyContent: 'center'}]}>
          <ActivityIndicator size='large'/>
        </View>
      )
  }
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EEEEEE'
  },
  content: {
    flex: 1
  },
  input: {
    height: 50,
    width: 200
  },
  field: {
    width: '80%',
    height: 50,
    flexDirection: 'row',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 10,
    backgroundColor: 'rgba(255, 255, 255, 0.4)',
    elevation: 1,
    borderRadius: 10
  },
  label: {
    height: 50,
    width: 80,
    marginLeft: 5
  },
  button: {
    width: 200,
    height: 50,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 30,
    borderWidth: 1,
    borderStyle: 'solid',
    backgroundColor: '#00C853',
    borderColor: 'white',
    borderRadius: 30
  },
  buttonLoading: {
    width: 50,
    height: 50,
    marginLeft: 'auto',
    marginRight: 'auto',
    marginTop: 30,
    borderWidth: 1,
    borderStyle: 'solid',
    backgroundColor: '#00C853',
    borderColor: 'white',
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center'
  },
  avatarView: {
    width: '80%',
    marginLeft: 'auto',
    marginRight: 'auto',
    height: 160,
    marginTop: 10
  },
  avatar: {
    width: 150,
    height: 150,
    marginLeft: 'auto',
    marginRight: 'auto',
    borderRadius: 75
  }
})