import React, { Component } from 'react'
import {Callout} from 'react-native-maps'
import {Text} from 'react-native'
import { Actions } from 'react-native-router-flux'

export default class CalloutCustom extends Component {
  constructor(props) {
    super(props);
    this.state = {
      district: '',
      ward: ''
    }
  }
  
  async componentDidMount(){
    const {idDistrict: districtId, idWard: wardId} = this.props.marker;
    let resDistrict = await fetch(`http://food-delivery-server.herokuapp.com/district/getAll`)
    let resWard = await fetch(`http://food-delivery-server.herokuapp.com/ward/getAllByDistrict?id=${districtId}`);
    let districts = await resDistrict.json();
    let wards = await resWard.json();
    
    let district = (districts.find(item => item.id === districtId))
    let ward = (wards.find(item => item.id === wardId));
    this.setState({
      district: district.name,
      ward: ward.name
    })

  }
  
  onPressCallout = marker => Actions.push('merchantdetail', { id: marker.RESTAURANT.id, name: marker.RESTAURANT.name, timeOpen: marker.RESTAURANT.timeOpen, timeClose: marker.RESTAURANT.timeClose, rating: marker.RESTAURANT.rating, image: marker.RESTAURANT.image })
  
  
  render() {
    const {marker} = this.props;
    return (
      <Callout style={{ width: 150, flex: 1, alignItems: 'center', justifyContent: 'center' }} onPress={() => this.onPressCallout(marker)}>
        <Text style={{ textAlign: 'center', textAlignVertical: 'center', fontSize: 16, fontWeight: '800' }}>
          {marker.RESTAURANT.name}
        </Text>
        <Text style={{ textAlign: 'center', textAlignVertical: 'center', fontSize: 12 }}>
          {marker.street}, {this.state.ward}, {this.state.district}
        </Text>
      </Callout>
    )
  }
}
