import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  Dimensions,
  Image,
  ImageBackground
} from 'react-native';

import MapView, { Marker, ProviderPropType, Callout } from 'react-native-maps';
import CalloutCustom from './CalloutCustom';


const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 10.762968768844209;
const LONGITUDE = 106.68228277936578
const LATITUDE_DELTA = 0.02
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
let id = 0;

function randomColor() {
  return `#${Math.floor(Math.random() * 16777215).toString(16)}`;
}

class Map extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      markers: [],
      loading: false
    };
  }

  onRegionChange = (region) => {
    if (!this.state.loading) {
      this.setState({ loading: true }, async () => {
        // let res = await fetch(`http://food-delivery-server.herokuapp.com/restaurant/nearMe/${10.773533}&${106.702899}`)
        let res = await fetch(`http://food-delivery-server.herokuapp.com/restaurant/nearMe/${region.latitude}&${region.longitude}`)
        let data = await res.json();
        if (data.length > 0) {
          let list = [];
          data.forEach(item => {
            if (!this.state.markers.find(i => item.id === i.id)) {
              list.push(item)
            }
          })
          this.setState({
            markers: [...this.state.markers, ...list],
            loading: false
          })
        } else
          this.setState({
            loading: false
          })
      })

    }
  }

  onMapPress = e => {
    console.log(e.nativeEvent)
  }

  


  render() {
    return (
      <View style={styles.container}>
        <MapView
          provider={'google'}
          style={styles.map}
          initialRegion={this.state.region}
          onPress={this.onMapPress}
          onRegionChange={this.onRegionChange}
        >
          {this.state.markers.map((marker, index) => {
            return <Marker
              key={index}
              coordinate={{ longitude: marker.longitude, latitude: marker.latitude }}
              pinColor={'red'}
            >
              <CalloutCustom marker={marker} />
            </Marker>
          })
          }
        </MapView>

      </View>
    );
  }
}
Map.propTypes = {
  provider: ProviderPropType,
};

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  bubble: {
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  latlng: {
    width: 200,
    alignItems: 'stretch',
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: 'center',
    marginHorizontal: 10,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: 20,
    backgroundColor: 'transparent',
  },
});

export default Map;