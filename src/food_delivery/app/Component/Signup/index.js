import React from 'react'
import {View, Image, StyleSheet, TouchableOpacity, TextInput, Text, Alert} from 'react-native'

import logo from '../../Image/fd_logo.jpg'
import validator from 'email-validator'
import {Actions} from 'react-native-router-flux'
import I18n from '../../I18n'
import Icon from 'react-native-vector-icons/FontAwesome';
import { fnSignup } from '../../API/Account';

class SignUp extends React.Component {
  constructor(props){
    super(props);;
    this.state = {
      email: '',
      password: '',
      confirm: '',
      validEmail: false
    }
  }

  validateEmail = () => {
    if(!validator.validate(this.state.email)){
      return this.setState({validEmail: false}, () => Alert.alert(I18n.t('signupError'), I18n.t('emailInvalid')))
    }
    else this.setState({validEmail: true})

  }

  handleInput = (text, name) => {
    let state = this.state;
    state[name] = text;
    this.setState(state);
  }

  validateInfo = () => {
    const {password, confirm, validEmail} = this.state;
    if(password.length === 0 || confirm !== password || !validEmail)
      return false;
    return true;
  }

  submit = () => {
    let valid = this.validateInfo();
    if(!valid){
      Alert.alert(I18n.t('signupError'), I18n.t('infoInvalid'));
      return;
    }
    return fnSignup({email: this.state.email, password: this.state.password}).then(res => {
      Alert.alert(I18n.t('signupSuccess'), I18n.t('signupSuccessMessage'));
      Actions.push('login');
    }).catch(err => {
      Alert.alert(I18n.t('signupError'), I18n.t('signupErrorMessage'));
    })
  }

  render () {
    return(
      <View style={Styles.container}>
        <View style={Styles.logo}>
          <Image source={logo} style={Styles.loginImg}/>
        </View>
        <View style={Styles.form}>
          <View style={Styles.inputWrapper}>
            <Icon name="envelope" sise={22} style={Styles.inlineImg}/>
            <TextInput placeholder={I18n.t('email')} style={Styles.input} keyboardType='email-address' textContentType='emailAddress' value={this.state.email} onChangeText={text => this.handleInput(text, 'email')} onEndEditing={() => this.validateEmail()}/>
          </View>
          <View style={Styles.inputWrapper}>
            <Icon name="lock" size={30} style={Styles.inlineImg}/>
            <TextInput placeholder={I18n.t('password')} style={Styles.input} secureTextEntry={true} onChangeText={text => this.handleInput(text, 'password')} value={this.state.password}/>
          </View>
          <View style={Styles.inputWrapper}>
            <Icon name="lock" size={30} style={Styles.inlineImg}/>
            <TextInput placeholder={I18n.t('confirmPassword')} style={Styles.input} secureTextEntry={true} onChangeText={text => this.handleInput(text, 'confirm')} value={this.state.confirm}/>
          </View>
          <TouchableOpacity style={Styles.loginBtn}>
            <Text style={Styles.loginText}>
              {I18n.t('signup')}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

const Styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: '#cecece',
    position: 'relative'
  },
  logo: {
    width: '100%',
    height: '50%',
    position: 'relative'
  },
  loginImg: {
    width: 150,
    height: 150,
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: [
      {
        translateX: -75
      }, {
        translateY: -75
      }
    ],
    borderRadius: 75
  },
  form: {
    width: '100%',
    height: 200,
    position: 'relative'
  },
  input: {
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    height: 40,
    marginHorizontal: 10,
    paddingLeft: 45,
    borderRadius: 20,
    color: '#000'
  },
  inputWrapper: {
    width: '90%',
    height: 40,
    position: 'relative',
    marginTop: 5,
    marginBottom: 5,
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  inlineImg: {
    position: 'absolute',
    width: 22,
    zIndex: 100,
    left: 22,
    lineHeight: 40
  },
  btnEye: {
    position: 'absolute',
    right: 20
  },
  loginBtn: {
    width: '50%',
    height: 50,
    position: 'relative',
    marginLeft: 'auto',
    marginRight: 'auto',
    backgroundColor: '#4caf50',
    marginTop: 30,
    borderRadius: 25,
    marginBottom: 0
  },
  loginText: {
    textAlign: 'center',
    fontSize: 18,
    lineHeight: 50,
    color: 'white'
  },
  footer: {
    position: 'relative',
    width: '90%',
    height: 30,
    marginLeft: 'auto',
    marginRight: 'auto',
    top: -10
  },
  text: {
    fontSize: 14,
    lineHeight: 30
  },
  createAccount: {
    position: 'absolute',
    top: 0,
    left: 5,
    width: '50%',
    height: 30
  },
  forgetPassword: {
    position: 'absolute',
    top: 0,
    right: 1,
    width: '50%',
    height: 30
  },
  btnLoading: {
    width: 50,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export default SignUp;