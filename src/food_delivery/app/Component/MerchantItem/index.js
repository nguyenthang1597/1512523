import React from 'react';
import { View, Text, Image, StyleSheet, Dimensions, TouchableOpacity } from 'react-native'
import { Actions } from 'react-native-router-flux';
let width = Dimensions.get('window').width;
let itemWidth = (width - 30) / 2;
const style = StyleSheet.create({
    item: {
        height: 260,
        width: itemWidth,
        flexDirection: 'column',
        backgroundColor: '#F5F5F6',
        marginLeft: 10,
        marginRight: 10,
        marginTop: 5,
        marginBottom: 5,
        elevation: 2,
        borderRadius: 10
    },
    img: {
        width: (itemWidth - 10),
        height: (itemWidth - 10),
        margin: 5
    }
})


export default (props) => {
    const {index} = props;
    return (
        <TouchableOpacity style={[style.item, index % 2 === 0 ? {marginRight: 5} : {marginLeft: 5}]} onPress={() => Actions.push('merchantdetail', { id: props.item.id})}>
            <Image style={style.img} source={{uri: `${props.item.image}`}} />
            <View style={{flex: 1, flexDirection: 'column' }} >
                <View style={{flex: 2, top:0, zIndex: 5}} >
                    <Text style={{paddingLeft:5, fontSize: 16, fontWeight: 'bold', marginBottom: 5}}>{props.item.name}</Text>
                </View>
            </View>
        </TouchableOpacity>
    )
}
