import React from 'react'
import {View, Text} from 'react-native'
export default ({focused, displayIcon}) => {
  return (
    <View style={{backgroundColor: '#ffa726', width: '100%', height: '100%', justifyContent: 'center'}}>
    <Text style={{color: focused ? 'white': 'black', textAlign: 'center'}}>
      {
        displayIcon
      }
    </Text>
  </View>
  )
}