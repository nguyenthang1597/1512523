import React, { Fragment } from 'react'
import { View, StyleSheet, Text, Image, ActivityIndicator } from 'react-native'
import I18n from '../../I18n'
import { GetFood } from '../../API/Order';
export default class OrderItem extends React.Component {
  state = {
    image: null,
    name: null,
    loading: true,
    price: 0
  }

  componentDidMount() {
    return GetFood(this.props.item.idFood)
      .then(res => this.setState({
        image: res.data.food.image,
        name: res.data.food.name,
        loading: false,
        price: res.data.food.price
      }))
      .catch(e => {
        this.setState({ image: null, name: null, loading: false })
      })
  }
  render() {
    return (
      <View style={style.item}>
        {
          this.state.loading && 
          <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <ActivityIndicator size={32} />
          </View>
        }
        {
          !this.state.loading &&
          <Fragment>
            <View style={style.image}>
              <Image style={{ width: 100, height: 100, borderRadius: 10 }} source={{ uri: `${this.state.image}` }} />
            </View>
            <View style={style.infosection}>
              <View style={{ flex: 3 }}>
                <View style={{ flex: 1 }}>
                  <Text style={{ fontSize: 18, fontWeight: 'bold', marginLeft: 10, marginTop: 5, color: 'black' }}>{this.state.name}</Text>
                </View>
                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                  <Text style={{ lineHeight: 50, fontSize: 20, marginLeft: 10 }}>{I18n.t('quan')}: {this.props.item.quantity} - {this.props.item.quantity * this.state.price} VND</Text>
                </View>
              </View>
            </View>
          </Fragment>
        }
      </View>
    )
  }
}

const style = StyleSheet.create({
  item: {
    flexDirection: 'row',
    height: 100,
    marginTop: 5,
    marginBottom: 5,
    elevation: 1,
    backgroundColor: 'white',
    borderRadius: 10
  },
  image: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 10
  },
  infosection: {
    flex: 3,
    flexDirection: 'row',
    marginLeft: 10
  },
})