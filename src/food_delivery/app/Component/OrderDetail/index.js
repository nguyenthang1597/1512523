import React from 'react';
import { View, Text, StyleSheet, TextInput, FlatList, Alert } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import moment from 'moment'
import I18n from '../../I18n'
import OrderItem from './OrderItem'
import { GetOrder } from '../../API/Order';
export default class OrderDetail extends React.Component {
  state = {
    list: []
  }
  componentDidMount(){
    return GetOrder(this.props.token, this.props.id)
    .then(res => this.setState({list: res.data.details}))
    .catch(e => this.setState({list: []}))
  }
  render() {
    return (
      <View style={Style.container}>
        <View style={Style.inputWrapper}>
          <Icon name='map-marker' size={22} style={Style.inlineImg} />
          <TextInput style={Style.input} editable={false} placeholder={I18n.t('orderAddress')} value={this.props.address} />
        </View>
        <View style={Style.inputWrapper}>
          <Icon name='phone' size={22} style={Style.inlineImg} />
          <TextInput style={Style.input} editable={false} placeholder={I18n.t('orderPhone')} value={this.props.phone} />
        </View>
        <View style={Style.inputWrapper}>
          <Icon name='credit-card' size={22} style={Style.inlineImg} />
          <TextInput style={Style.input} editable={false} value={this.props.totalPrice + ' VND'} />
        </View>
        <View style={Style.inputWrapper}>
          <Icon name='calendar' size={22} style={Style.inlineImg} />
          <TextInput style={Style.input} editable={false} value={moment(this.props.date).format('DD/MM/YYYY - HH:mm:ss')} />
        </View>
        <View style={{ flex: 1 }}>
          <FlatList
            data={this.state.list}
            renderItem={({ item, index }) => <OrderItem key={index} item={item} />}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </View>
    );
  }
}



const Style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#cecece'
  },
  input: {
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    height: 40,
    marginHorizontal: 10,
    paddingLeft: 45,
    borderRadius: 20,
    color: '#000'
  },
  inputWrapper: {
    width: '90%',
    height: 40,
    position: 'relative',
    marginTop: 5,
    marginBottom: 5,
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  inlineImg: {
    position: 'absolute',
    width: 25,
    zIndex: 100,
    left: 22,
    lineHeight: 40
  },
})



