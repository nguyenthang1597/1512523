import React from 'react';
import { View, Image, Text, TouchableOpacity } from 'react-native';
import {Actions} from 'react-native-router-flux'

export default ({id, name, image, price, resId}) => {
    console.log('MenuItem', resId)
    return (
        <TouchableOpacity style={{ height: 350, width: 200, margin: 5, flexDirection: 'column', elevation: 5}} onPress={() => Actions.fooddetail({id, fname: name, fprice: price ,fimage: image, resId})}>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Image style={{ width: 120, height: 120}} source={{uri: `${image}`}} />
            </View>
            <View style={{ flex: 2, marginTop: 20 }}>
                <Text style={{textAlign: 'center'}}>{name}</Text>
                <Text style={{textAlign: 'center'}}>{price} VND</Text>
            </View>
        </TouchableOpacity>
    )
}