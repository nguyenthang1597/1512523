import React from 'react'
import { View, Text, ActivityIndicator, StyleSheet, Image, TouchableOpacity, WebView , FlatList} from 'react-native'
import { fnGetMerchantById, fnGetComment } from '../../API/Merchant'
import Icon from 'react-native-vector-icons/FontAwesome';
import { Rating } from 'react-native-elements';
import Swiper from 'react-native-swiper'
import MenuItem from './MenuItem'
import { Actions } from 'react-native-router-flux';
class MerchantDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            resource: [],
            restaurant: {},
            address: {},
            menu: [],
            loading: true,
            comments: []
        }
    }

    componentDidMount() {
        const { id } = this.props;
        Promise.all([fnGetMerchantById(id), fnGetComment(id)])
            .then(res => this.setState({
                ...res[0].data,
                comments: res[1].data.result,
                loading: false
            }))
    }

    render() {
        const { resource, restaurant, address, menu, loading, comments } = this.state;
        return (
            <View style={style.container}>
                {
                    loading ? 
                    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                        <ActivityIndicator size='large' />
                    </View>
                        :
                        <View style={style.body}>
                            <View style={style.merchantImage}>
                                <Swiper>
                                    {
                                        resource.map((e, index) => e.type === 'image' ? <Image key={index} style={{ width: "100%", height: '100%' }} source={{ uri: e.url }} /> : <WebView key={index} source={{ uri: e.url }} />)
                                    }
                                </Swiper>
                            </View>
                            <View style={style.merchantInfo}>
                                <View style={{ flexDirection: 'row', height: 80 }}>
                                    <View style={{ flex: 1 }}>
                                        <Text numberOfLines={1} style={{ lineHeight: 40, fontSize: 18, fontWeight: 'bold', textAlign: 'center', overflow: 'hidden' }}>{restaurant.name}</Text>
                                    </View>
                                </View>
                                <View style={{ height: 30, paddingLeft: 5, flexDirection: 'row', alignItems: 'center' }}>
                                    <Icon name='map-marker' size={30} />
                                    <Text numberOfLines={2} style={{ marginLeft: 5, marginRight: 5, lineHeight: 15 }} >{address.address}</Text>
                                </View>
                                <View style={{ height: 30, paddingLeft: 5, flexDirection: 'row' }}>
                                    <View style={{ flexDirection: 'row', flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={{ lineHeight: 30 }}>Đánh giá: </Text>
                                        <Rating
                                            type="star"
                                            startingValue={restaurant.rating}
                                            readonly
                                            imageSize={20}
                                            style={{ paddingVertical: 'auto' }}
                                        />
                                    </View>
                                    <TouchableOpacity onPress={() => Actions.comments({id: this.props.id, name: this.state.restaurant.name, comments: this.state.comments})} style={{ flexDirection: 'row', flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                        <Text style={{ lineHeight: 30, textAlign: 'right' }}>Bình luận: {comments.length}</Text>
                                    </TouchableOpacity>
                                    
                                    
                                </View>
                                <Text style={{ fontWeight: 'bold', fontSize: 20, marginLeft: 20 }}>Menu</Text>
                                <View style={{ height: 240, marginTop: 5,  justifyContent: 'center' }}>
                                    <FlatList 
                                        style={{alignContent: 'center'}}
                                        horizontal
                                        data={menu}
                                        renderItem={({item}) => <MenuItem resId={this.props.id} {...item}/>}
                                        keyExtractor={(item, index) => index.toString()}
                                    />
                                </View>
                            </View>

                        </View>
                }
            </View>
        )
    }
}


const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#BDBDBD'
    },
    body: {
        flex: 1,
    },
    merchantImage: {
        flex: 2
    },
    merchantInfo: {
        flex: 3,
        backgroundColor: 'white',
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15,
    }
})

export default MerchantDetail;
