import React from 'react'
import { View, StyleSheet, Text, Image } from 'react-native'
import I18n from '../../I18n'
export default (props) => {
  return (
    <View style={style.item}>
      <View style={style.image}>
        <Image style={{ width: 100, height: 100, borderRadius: 10 }} source={{ uri: `${props.item.image}` }} />
      </View>
      <View style={style.infosection}>
        <View style={{ flex: 3 }}>
          <View style={{ flex: 1 }}>
            <Text style={{ fontSize: 18, fontWeight: 'bold', marginLeft: 10, marginTop: 5, color: 'black' }}>{props.item.name}</Text>
          </View>
          <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text style={{ lineHeight: 50, fontSize: 20, marginLeft: 10 }}>{I18n.t('quan')}: {props.item.count} - {props.item.count * props.item.price} VND</Text>
          </View>
        </View>
      </View>
    </View>
  )
}

const style = StyleSheet.create({
  item: {
    flexDirection: 'row',
    height: 100,
    marginTop: 5,
    marginBottom: 5,
    elevation: 1,
    backgroundColor: 'white',
    borderRadius: 10
  },
  image: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 10
  },
  infosection: {
    flex: 3,
    flexDirection: 'row',
    marginLeft: 10
  },
})