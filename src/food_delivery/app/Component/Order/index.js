import React from 'react';
import { View, Text, StyleSheet, TextInput, TouchableOpacity, FlatList, Alert } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import OrderItem from './OrderItem';
import {Create} from '../../API/Order'
import {Actions} from 'react-native-router-flux'
import I18n from '../../I18n'
export default class Order extends React.Component {
  state = {
    address: '',
    phone: ''
  }

  handleTextInput = (name, value) => {
    let state = { ...this.state };
    state[name] = value;
    this.setState(state);
  }

  handleSubmit = async () => {
    const {address, phone} = this.state;
    if(!address || !phone){
      Alert.alert(I18n.t('orderError'), I18n.t('uncomplete'));
    }
    if(this.props.list.length === 0)
      return;
    let item = this.props.list.map(i => ({idFood: i.id, quantity: i.count, note: i.note}))
    let totalPrice = this.props.list ? this.props.list.map(i => i.price * i.count).reduce(sum) : 0;
    let data = {
      totalPrice,
      address,
      phone,
      idRestaurant: this.props.idRes,
      item
    }

    try {
      const res = await Create(this.props.token, data);
      this.props.clear();
      Alert.alert(I18n.t('orderSuccess', I18n.t('orderSuccessMsg')));
      setTimeout(() => Actions.Main(), 100);
    }
    catch (e) {
      Alert.alert(I18n.t('orderError'), I18n.t('orderErrorMsg'));
    }

  }

  render() {
    let total = this.props.list ? this.props.list.map(i => i.price * i.count).reduce(sum) : 0;
    return (
      <View style={Style.container}>
        <View style={Style.inputWrapper}>
          <Icon name='map-marker' size={22} style={Style.inlineImg} />
          <TextInput style={Style.input} placeholder={I18n.t('orderAddress')} onChangeText={text => this.handleTextInput('address', text)} value={this.state.address} />
        </View>
        <View style={Style.inputWrapper}>
          <Icon name='phone' size={22} style={Style.inlineImg} />
          <TextInput style={Style.input} placeholder={I18n.t('orderPhone')} onChangeText={text => this.handleTextInput('phone', text)} value={this.state.phone} />
        </View>
        <View style={Style.inputWrapper}>
          <Icon name='credit-card' size={22} style={Style.inlineImg} />
          <TextInput style={Style.input} editable={false} value={total.toString() + 'VND'} />
        </View>
        <TouchableOpacity style={Style.btn} onPress={() => this.handleSubmit()}>
          <Text style={Style.btnText}>{I18n.t('orderTitle')}</Text>
        </TouchableOpacity>
        <View style={{ flex: 1 }}>
          <FlatList
            data={this.props.list}
            renderItem={({ item, index }) => <OrderItem key={index} item={item} />}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      </View>
    );
  }
}



const Style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#cecece'
  },
  input: {
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    height: 40,
    marginHorizontal: 10,
    paddingLeft: 45,
    borderRadius: 20,
    color: '#000'
  },
  inputWrapper: {
    width: '90%',
    height: 40,
    position: 'relative',
    marginTop: 5,
    marginBottom: 5,
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  btn: {
    width: '50%',
    height: 50,
    position: 'relative',
    marginLeft: 'auto',
    marginRight: 'auto',
    backgroundColor: '#4caf50',
    marginTop: 10,
    borderRadius: 25,
    marginBottom: 0
  },
  btnText: {
    textAlign: 'center',
    fontSize: 18,
    lineHeight: 50,
    color: 'white'
  },
  inlineImg: {
    position: 'absolute',
    width: 25,
    zIndex: 100,
    left: 22,
    lineHeight: 40
  },
})



const sum = (a = 0, b = 0) => a + b;
