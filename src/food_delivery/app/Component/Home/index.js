import React, { Component } from 'react'
import { View, Text, StyleSheet, Animated, FlatList, RefreshControl, ActivityIndicator } from 'react-native'
import CategorySlide from '../CategorySlide'
import ItemList from '../MerchantItem'
import I18n from '../../I18n'
const SLIDE_HEIGHT = 160;
const SLIDE_COLLAPSED_HEIGHT = 0;

export default class Home extends Component {
  constructor() {
    super();

    this.state = {
      scrollY: new Animated.Value(0)
    }
  }
  componentDidMount() {
    if(!this.props.merchants.length)
      this.props.loadMerchants(1, 10);
    if(!this.props.categories.length)
      this.props.loadCate();
    this.props.getDistricts();
    this.props.getInfo(this.props.token)
  }
  _onEndReached = () => {
		const { nextPage } = this.props;
		if (nextPage == -1)
			return;
		this.props.loadMore(nextPage, 10)
	}
  render() {
    const { merchants, loading, refresh, refreshMerchants } = this.props;
    const headerHeight = this.state.scrollY.interpolate({
      inputRange: [0, SLIDE_HEIGHT - SLIDE_COLLAPSED_HEIGHT],
      outputRange: [SLIDE_HEIGHT, SLIDE_COLLAPSED_HEIGHT],
      extrapolate: 'clamp'
    });
    return (
      <View style={style.container}>
        <Animated.View style={{ height: headerHeight }}>
          <CategorySlide categories={this.props.categories}/>
        </Animated.View>
        <View style={style.body}>
					<View style={{width: '100%', backgroundColor: '#E1E2E1'}}>
						<Text style={{color: '#ffa726', fontSize: 20, marginLeft: 5}}>{I18n.t('restaurant')}</Text>
					</View>
					{loading ? <ActivityIndicator size={60} /> :
						<FlatList contentContainerStyle={style.list}
							refreshControl={
								<RefreshControl
								refreshing={refresh}
								onRefresh={() => refreshMerchants(1,10)}
								/>
							}
              onScroll={Animated.event(
                [{ nativeEvent: {
                    contentOffset: {
                      y: this.state.scrollY
                    }
                  }
                }])
              }
              scrollEventThrottle={60}
							onEndReached={this._onEndReached}
							onEndReachedThreshold={0.1}
							numColumns={2}
							data={merchants}
							renderItem={({ item, index }) => <ItemList key={index} index={index} item={item.info} />}
							keyExtractor={( item, index) => index.toString()}
						/>}
					{
						this.props.loadmore ? <View style={{top: '90%', left: '50%',position: 'absolute', flex: 1, justifyContent: 'center' , width: 40, height: 40, borderRadius: 20, backgroundColor: 'white', transform: [{translateX: -20}], shadowRadius: 20, shadowOpacity: 0.75, shadowOffset: {height: 1}, shadowColor: 'black', elevation: 5}}><ActivityIndicator size={30} color='black'/></View> : null
					}
				</View>
      </View>
    )
  }
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E1E2E1'
  },

  body: {
    flex: 1,
    justifyContent: "center",
    position: 'relative'
  },
  list: {
    flexDirection: 'column',
    position: 'relative',
    backgroundColor: '#E1E2E1'
  },
  item: {
    backgroundColor: 'red',
    margin: 3,
    width: 100,
  }
})
