import React, {Fragment} from 'react'
import {View, StyleSheet, Image, Text, TouchableOpacity, ActivityIndicator} from 'react-native'
import Swiper from 'react-native-swiper';
import {Actions} from 'react-native-router-flux'
import _ from 'lodash';
import I18n from '../../I18n'
class CategorySlide extends React.Component {
  render() {
    let categories = this.props.categories || [];
    let list = _.chunk(categories, 3);
    return (
      <View style={Styles.container}>

          <View>
            <Text style={Styles.headerText}>{I18n.t('category')}</Text>
          </View>

        {
          list.length <= 0 && <ActivityIndicator color='black' size={30} style={{top: '50%'}}/>
        }

        {
          list.length > 0 &&
          <Swiper autoplay={true} loop={true} showsPagination={false} autoplayTimeout={4}>
              {
                list.map((row, rowIndex) =>
                <View style={{ marginTop: 5, width: '100%',height: 100,flexDirection: 'row',alignItems: 'stretch',justifyContent: 'space-around'}} key={rowIndex}>
                  {
                    row.map((i, index) =>
                      <TouchableOpacity key={index} style={{position: 'relative'}} onPress={() => Actions.push('category', {id: i.id, title: i.name})}>
                        <Image source={{uri: i.image}} style={{width: 80, height: 80, borderRadius: 40}}/>
                        <Text style={{textAlign: 'center', position: 'relative'}}>{i.name}</Text>
                      </TouchableOpacity>)
                  }
                </View>)
              }
            </Swiper>
        }

      </View>)
  }
}

const Styles = StyleSheet.create({
  container: {
    width: '100%',
    position: 'relative',
    backgroundColor: '#E1E2E1',
    height: 140,
  },
  img: {
    width: '80%',
    height: 180,
    marginLeft: 'auto',
    marginRight: 'auto',
    top: 35
  },
  header: {
    left: 5
  },
  headerText: {
    fontSize: 20,
    color: 'orange',
    paddingLeft: 5,
  }
})

export default CategorySlide;
