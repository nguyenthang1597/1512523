import React from 'react'
import { View, Text, FlatList, TouchableOpacity, StyleSheet } from 'react-native'
import BasketItem from './BasketItem';
import {Actions} from 'react-native-router-flux'
import I18n from '../../I18n'


export default ({data, increase, decrease, deleteItem}) => {
  let _data = data || [];
  return (
    <View style={style.container}>
        <View style={style.body}>
            <View style={{ flexDirection: 'row', height: 40 }}>
                <View style={{ flex: 1 }}>
                    <Text style={{ fontSize: 30, fontWeight: 'bold', marginLeft: 10, lineHeight: 40, color: 'black' }}>{I18n.t('cart')}</Text>
                </View>
            </View>
            <View style={{ flex: 1 }}>
                {
                    <FlatList
                        data={_data}
                        renderItem={({ item }) => <BasketItem item={item} increase={increase} decrease={decrease} deleteItem={deleteItem}/>}
                        keyExtractor={(item, index ) => index.toString()}
                    />
                }

            </View>
            <TouchableOpacity disabled={_data.length === 0} style={{ height: 60, margin: 5, backgroundColor: '#00C853', borderRadius: 40 }} onPress={() => Actions.order()}>
                <Text style={{ lineHeight: 60, fontSize: 20, color: 'white', textAlign: 'center' }}>{I18n.t('total')}: {_data.length > 0 ? _data.map(item => item.price * item.count).reduce(sum)  : 0}</Text>
            </TouchableOpacity>
        </View>
    </View>
);
}

const style = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#EEEEEE'
    },
    body: {
        flex: 1
    },
    active: {
        color: 'black'
    }
})


const sum = (a,b = 0) => a + b;

