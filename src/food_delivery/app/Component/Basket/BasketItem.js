import React from 'react'
import { View, StyleSheet, Text, TouchableOpacity, Image } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';

export default (props) => {
    return (
        <View style={style.item}>
            <View style={style.image}>
                <Image style={{ width: 100, height: 100, borderRadius: 10 }} source={{uri: `${props.item.image}`}} />
            </View>
            <View style={style.infosection}>
                <View style={{ flex: 3 }}>
                    <View style={{ flex: 1 }}>
                        <Text style={{ fontSize: 18, fontWeight: 'bold', marginLeft: 10, marginTop: 5,color: 'black' }}>{props.item.name}</Text>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                    <TouchableOpacity onPress={() => props.decrease(props.item.id, props.item.note)}>
                            <Icon name='minus-circle' size={30} style={{lineHeight:50}} color='yellow' />
                        </TouchableOpacity>
                        <Text style={{ lineHeight: 50, fontSize: 20 }}>x{props.item.count}</Text>
                        
                        <TouchableOpacity onPress={() => props.increase(props.item.id, props.item.note)}>
                            <Icon name='plus-circle' size={30} style={{lineHeight:50}} color='green'/>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ flex: 1 , alignItems: 'center', justifyContent: 'center'}}>
                    <TouchableOpacity onPress={() => props.deleteItem(props.item.id, props.item.note)}>
                        <Icon name='remove' size={30} color='red'/>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}

const style = StyleSheet.create({
    item: {
        flexDirection: 'row',
        height: 100,
        marginTop: 5,
        marginBottom: 5,
        elevation: 1,
        backgroundColor: 'white',
        borderRadius: 10
    },
    image: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 10
    },
    infosection: {
        flex: 3,
        flexDirection: 'row',
        marginLeft: 10
    },
    info: {
        flex: 5,
        flexDirection: 'column',
        justifyContent: 'space-between',
        marginTop: 15,
        marginBottom: 15
    },
    control: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end',
        marginTop: 15,
        marginBottom: 15
    }
})