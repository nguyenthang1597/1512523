import React from 'react'
import { View, Text, Image, TextInput, TouchableOpacity, StyleSheet, ActivityIndicator, Alert } from 'react-native'
import logo from '../../Image/fd_logo.jpg'
import I18n from '../../I18n'
import Icon from 'react-native-vector-icons/FontAwesome';
import {Actions} from 'react-native-router-flux';
class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showPassword: true,
      info: {
        email: 'nguyenthang1597@gmail.com',
        password: '123456'
      }
    }
  }

  handleTextInput = (name, val) => {
    const { info } = this.state;
    info[name] = val;
    this.setState({ info })
  }

  toggleShowPassword = () => this.setState({
    showPassword: !this.state.showPassword
  })

  submit = () => {
    const {info} = this.state;
    this.props.authenticate(info)
  }

  componentDidMount(){
    if(this.props.isAuthenticated){
      setTimeout(() => {
        Actions.Main({type: 'reset'})
      }, 1000)
      
    }
  }

  render() {
    const { showPassword } = this.state;
    const { isAuthenticating, hasError } = this.props;
    if (hasError)
      Alert.alert(I18n.t('loginError'), I18n.t('loginFailed'), [
        {
          text: 'OK',
          onPress: () => this.props.closeDialog()
        }
      ], { cancelable: false })
    return (
      this.props.isAuthenticated ? 
      <View style={[Styles.container, {justifyContent: 'center', alignItems: 'center'}]}>
        <ActivityIndicator size="large"/>
        <Text style={{fontSize: 20}}>{I18n.t('loading')}</Text>
      </View> 
      : <View style={Styles.container}>
      <View style={Styles.logo}>
        <Image source={logo} style={Styles.loginImg} />
      </View>
      <View style={Styles.form}>
        <View style={Styles.inputWrapper}>
          <Icon name='envelope' size={22} style={Styles.inlineImg} />
          <TextInput placeholder={I18n.t('email')} style={Styles.input} value={this.state.info.email} onChangeText={text => this.handleTextInput('email', text)} />
        </View>
        <View style={Styles.inputWrapper}>
          <Icon name='lock' size={30} style={Styles.inlineImg} />
          <TextInput placeholder={I18n.t('password')} style={Styles.input} secureTextEntry={showPassword} value={this.state.info.password} onChangeText={text => this.handleTextInput('password', text)} />
          <TouchableOpacity style={Styles.btnEye} onPress={() => this.toggleShowPassword()}>
            <Icon name="eye" size={30} style={{lineHeight: 40}}/>
          </TouchableOpacity>
        </View>
        <TouchableOpacity style={[Styles.loginBtn, isAuthenticating? Styles.btnLoading: null]} onPress={() => this.submit()}>
          {
            !isAuthenticating && <Text style={Styles.loginText}>
              {I18n.t('login')}
            </Text>
          }
          {isAuthenticating && <ActivityIndicator size={20} color='white' />}
        </TouchableOpacity>
      </View>
      <View style={Styles.footer}>
        <TouchableOpacity style={Styles.createAccount} onPress={() => Actions.push('signup')}>
          <Text style={Styles.text}>{I18n.t('createAccount')}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={Styles.forgetPassword} onPress={() => Actions.forgetpw()}>
          <Text style={[
            Styles.text, {
              textAlign: 'right'
            }
          ]}>{I18n.t('forgetPassword')}</Text>
        </TouchableOpacity>
      </View>
    </View>
      )
  }
}
const Styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: '#cecece',
    position: 'relative'
  },
  logo: {
    width: '100%',
    height: '50%',
    position: 'relative'
  },
  loginImg: {
    width: 150,
    height: 150,
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: [
      {
        translateX: -75
      }, {
        translateY: -75
      }
    ],
    borderRadius: 75
  },
  form: {
    width: '100%',
    height: 200,
    position: 'relative'
  },
  input: {
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
    height: 40,
    marginHorizontal: 10,
    paddingLeft: 45,
    borderRadius: 20,
    color: '#000'
  },
  inputWrapper: {
    width: '90%',
    height: 40,
    position: 'relative',
    marginTop: 5,
    marginBottom: 5,
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  inlineImg: {
    position: 'absolute',
    width: 22,
    zIndex: 100,
    left: 22,
    lineHeight: 40
  },
  btnEye: {
    position: 'absolute',
    right: 20
  },
  loginBtn: {
    width: '50%',
    height: 50,
    position: 'relative',
    marginLeft: 'auto',
    marginRight: 'auto',
    backgroundColor: '#4caf50',
    marginTop: 30,
    borderRadius: 25,
    marginBottom: 0
  },
  loginText: {
    textAlign: 'center',
    fontSize: 18,
    lineHeight: 50,
    color: 'white'
  },
  footer: {
    position: 'relative',
    width: '90%',
    height: 30,
    marginLeft: 'auto',
    marginRight: 'auto',
    top: -10
  },
  text: {
    fontSize: 14,
    lineHeight: 30
  },
  createAccount: {
    position: 'absolute',
    top: 0,
    left: 5,
    width: '50%',
    height: 30
  },
  forgetPassword: {
    position: 'absolute',
    top: 0,
    right: 1,
    width: '50%',
    height: 30
  },
  btnLoading: {
    width: 50,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export default Login;
