import React from 'react'
import {View, Text, Image, Dimensions} from 'react-native'
let width = Dimensions.get('window').width;
const NotiItem = ({title, image, content}) => {
  return (
    <View style={{width: width - 10, marginLeft: 'auto', marginRight: 'auto', height: 100, flexDirection: 'row', backgroundColor: 'white', marginTop: 5, elevation: 3, borderRadius: 10}}>
      <View style={{width: 100, height: 100, justifyContent: 'center', alignItems: 'center'}}>
        <Image source={{uri: image}}/>
      </View>
      <View style={{flexDirection: 'column', flex: 1}}>
        <View style={{height: 40}}>
          <Text style={{lineHeight: 40, fontSize: 20, fontWeight: '800'}}>{title}</Text>
        </View>
        <View style={{height: 60}}>
          <Text numerOfLines={3} ellipsizeMode={'clip'} style={{lineHeight: 20, fontSize: 16, }}>{content}</Text>
        </View>
      </View>
    </View>
  )
}

export default NotiItem;