import React from 'react'
import {View, Text, Image, StyleSheet, ActivityIndicator} from 'react-native'
import NotiItem from './NotiItem'

class Notification extends React.Component {
  componentDidMount() {
    const {fetchNoti, token} = this.props;
    fetchNoti(token);
  }
  render () {
    const {list, loading} = this.props;
    return(
      <View style={style.container}>
        {
          loading && 
          <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <ActivityIndicator size='large'/>
          </View>
        }
        {
          !loading && list.map((e, index)=> <NotiItem key={index} {...e} />)
        }
      </View>
    )
  }
}

export default Notification;

const style = StyleSheet.create({
	container: {
		flex: 1,
    backgroundColor: '#E1E2E1',
	},

	body: {
		flex: 1,
		justifyContent: "center",
		position:'relative'
	},
	list: {
		flexDirection: 'column',
		position: 'relative',
    backgroundColor: '#E1E2E1'
	}
})