import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Alert } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux'
import I18n from '../../I18n'
const Setting = (props) => {
  return (
    <View style={Style.container}>
      <View style={{ height: 60, width: '100%', backgroundColor: 'white', marginTop: 5, flexDirection: 'row', alignItems: 'center' }}>
        <TouchableOpacity style={{ flex: 1, flexDirection: 'row' }} onPress={() => Actions.orderhistory()}>
          <Icon name='history' size={30} style={Style.icon} />
          <Text style={Style.label}>{I18n.t('orderhistory')}</Text>
        </TouchableOpacity>
      </View>
      <View style={{ height: 60, width: '100%', backgroundColor: 'white', marginTop: 5, flexDirection: 'row', alignItems: 'center' }}>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <Icon name='language' size={30} style={Style.icon} />
          <Text style={Style.label}>{I18n.t('language')}</Text>
        </View>
        <TouchableOpacity disabled={props.language === 'en'} onPress={() => props.changeLanguage('en')} style={[Style.languageBtn, { borderTopLeftRadius: 15, borderBottomLeftRadius: 15 }, props.language === 'en' ? Style.active : null]}><Text style={{ lineHeight: 30, textAlign: 'center', color: 'white' }}>{I18n.t('english')}</Text></TouchableOpacity>
        <TouchableOpacity disabled={props.language === 'vi'} onPress={() => props.changeLanguage('vi')} style={[Style.languageBtn, { borderTopRightRadius: 15, borderBottomRightRadius: 15 }, props.language === 'vi' ? Style.active : null]}><Text style={{ lineHeight: 30, textAlign: 'center', color: 'white' }}>{I18n.t('vietnamese')}</Text></TouchableOpacity>
      </View>
      <View style={{ height: 60, width: '100%', backgroundColor: 'white', marginTop: 5, flexDirection: 'row', alignItems: 'center' }}>
        <TouchableOpacity onPress={() => Actions.changepassword()} style={{ flex: 1, flexDirection: "row" }}>
          <Icon name='lock' size={30} style={Style.icon} />
          <Text style={Style.label}>{I18n.t('changepassword')}</Text>
        </TouchableOpacity>
      </View>





      <View style={{ height: 60, width: '100%', backgroundColor: 'white', marginTop: 5, flexDirection: 'row', alignItems: 'center' }}>
        <TouchableOpacity onPress={() => {
          Alert.alert(I18n.t('logout'), I18n.t('areyousure'), [
            { text: I18n.t('yes'), onPress: () => { props.logout(); props.clear(); Actions.login({ type: 'reset' }) } },
            { text: I18n.t('no'), style: 'cancel' }
          ],
            { cancelable: true })
        }} style={{ flex: 1, flexDirection: 'row' }}>
          <Icon name='sign-out' size={30} style={Style.icon} />
          <Text style={Style.label}>{I18n.t('logout')}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const Style = StyleSheet.create({
  container: {
    flex: 1
  },
  label: {
    lineHeight: 60,
    fontSize: 26,
    fontWeight: '800'
  },
  languageBtn: {
    width: 80,
    height: 30,
    backgroundColor: 'black',
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,.2)',
  },
  active: {
    backgroundColor: '#ffa726',
    elevation: 3
  },
  icon: {
    lineHeight: 60,
    marginRight: 10,
    marginLeft: 10
  }
})


export default Setting;